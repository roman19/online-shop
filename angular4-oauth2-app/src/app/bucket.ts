import {Product} from "./product"
import {Account} from "./account"

export class Bucket {
    constructor(
        public product: Product,
        public account: Account,
        public quantity: number
    ) { }
}