import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Product } from '../product';
import { AppService } from '../app.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./product-form.component.css']
})
export class EditProductComponent implements OnInit {

  httpStatus: number = undefined;

  product: Product = new Product("", 0);

  form;

  constructor(private _service: AppService, private _route: ActivatedRoute) { }

  ngOnInit() {
    this.httpStatus = undefined;

    var id = this._route.params.subscribe(params => {
      var id = params['id'];

      if (!id)
        return;

      this._service.getProductById(id)
      .then((product) => {
            this.product = new Product(product.name, product.price, id, id);
          })
          .catch((error: Response) => {
            console.log(error.status);
          });
    });

    this.form = new FormGroup({
      name: new FormControl(),
      price: new FormControl()
    });
  }

  onSubmit(product) {
    this._service.editProduct(this.product);
  }




  forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const forbidden = nameRe.test(control.value);
      return forbidden ? { 'forbiddenName': { value: control.value } } : null;
    };
  }

}
