import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { BucketItem } from '../bucketItem';
import {Account} from '../account';
import { Cookie } from 'ng2-cookies';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  httpStatus: number = undefined;
  
    bucketItems: Array<BucketItem> = [];
    account: Account;
  
    constructor(private _service: AppService) { }
  
    ngOnInit() {
      this.getUsersBucket();
      let user = this._service.getCurrentUser();
      let ss = user.user_name;
      this.account = new Account(user.user_name, user.name, user.surname);
    }
  
    getUsersBucket() {
      this._service.getUsersBucket()
        .then((bucketItems) => {
          this.bucketItems = bucketItems;
          this.httpStatus = 200;
        })
        .catch((error: Response) => {
          console.log(error.status);
          this.httpStatus = error.status;
        });
    }

}
