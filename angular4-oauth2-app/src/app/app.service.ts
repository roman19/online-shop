import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Product } from './product';
import { JwtHelper } from 'angular2-jwt';
import { Bucket } from './bucket';
import { Account } from './account';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs';

@Injectable()
export class AppService {
  
  private prodsUrl = 'http://localhost:8765/api/product-service/products';
  private bucketUrl = 'http://localhost:8765/api/bucket-service/accounts/';
  private authUrl = 'http://localhost:9090/oauth/token';
  private CLIENT_ID = 'online-shop-app';
  private CLIENT_SECRET = 'secret';
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private _router: Router, private _http: Http) { }

  receiveAccessToken(loginData) {
    const params = new URLSearchParams();
    params.append('username', loginData.username);
    params.append('password', loginData.password);
    params.append('grant_type', 'password');
    params.append('client_id', this.CLIENT_ID);

    const headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.CLIENT_ID + ':' + this.CLIENT_SECRET)
    });
    const options = new RequestOptions({ headers: headers});
    console.log(params.toString());
    this._http.post(this.authUrl, params.toString(), options)
      .map(res => res.json())
      .subscribe(
      data => this.saveToken(data),
      err => alert('Invalid Credentials: '+ err)
      );
  }

  saveToken(token) {
    const expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    Cookie.set("scope", token.scope, expireDate);
    localStorage.setItem('currentUser', JSON.stringify(this.jwtHelper.decodeToken(token.access_token)));
    this._router.navigate(['/']);
  }

  logout() {
    Cookie.delete('access_token');
    Cookie.delete('scope');
    this._router.navigate(['/login']);
  }

  checkCredentials() {
    if (!Cookie.check('access_token')) {
      this._router.navigate(['/login']);
    }
  }

  getRequestOptions(json: boolean): RequestOptions {
    let contentType = 'application/x-www-form-urlencoded; charset=utf-8';

    if (json) {
      contentType = 'application/json';
    }

    const headers = new Headers({
      'Content-type': contentType,
      'Authorization': 'Bearer ' + Cookie.get('access_token')
    });

    return new RequestOptions({ headers: headers });
  }


  // Products
  getProducts() {
    return this._http.get(this.prodsUrl, this.getRequestOptions(false))
      .map((res: Response) =>  res.json()._embedded.products).toPromise();
  }

  getProductById(id: string) {
    return this._http.get(this.prodsUrl + '/' + id, this.getRequestOptions(false))
      .map((res: Response) =>  res.json()).toPromise();
  }

  createProduct(product: Product) {
    const prod = new Product(product.name, product.price);
    return this._http.post(this.prodsUrl, JSON.stringify(prod), this.getRequestOptions(true))
      .map((res: Response) => res.json()).toPromise();
  }

  editProduct(product: Product) {
    return this._http.patch(this.prodsUrl + '/' + product.id, JSON.stringify(product), this.getRequestOptions(true))
      .map((res: Response) => res.json()).toPromise();
  }

  //Bucket
  getUsersBucket(){
    return this._http.get(this.bucketUrl+this.getCurrentUser().user_name, this.getRequestOptions(false))
    .map((res: Response) =>  res.json().bucketItems).toPromise();
  }

  addProductToUsersBucket(product: Product){
    const user = this.getCurrentUser();
    const prod = new Product(product.name, product.price, product.id);
    const quantity = 1;
    const account = new Account(user.user_name, user.name, user.surname);
    const bucket = new Bucket(prod, account, quantity);
    return this._http.post(this.bucketUrl+user.user_name, JSON.stringify(bucket), this.getRequestOptions(true))
      .map((res: Response) => res.json()).toPromise();
  }

  getCurrentUser(){
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
