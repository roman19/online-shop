export class Product {
    constructor(
        public name: string,
        public price: number,
        public mongoId?: string,
        public id?: string
    ) { }
}
