import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { JwtHelper } from 'angular2-jwt';
import { Cookie } from 'ng2-cookies'

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css']
})
export class NaviComponent implements OnInit {

  jwtHelper: JwtHelper = new JwtHelper();
  
  isAdmin = false;

  constructor(private _service: AppService) { }

  ngOnInit() {
    this._service.checkCredentials();

    
    const token: string = Cookie.get('access_token');
    if(token!=null && token!=''){
      let parsedJwtToken = this.jwtHelper.decodeToken(token);
      if(parsedJwtToken.authorities.indexOf("ROLE_ADMIN")>-1){
        this.isAdmin = true;
      }

    }else{
      console.log('User is unauthenticated')
    }
    
    
  }

  logout() {
    this._service.logout();
  }

}
