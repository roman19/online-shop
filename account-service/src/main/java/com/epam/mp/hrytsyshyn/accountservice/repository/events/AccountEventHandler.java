package com.epam.mp.hrytsyshyn.accountservice.repository.events;

import com.epam.mp.hrytsyshyn.accountservice.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

/**
 * Created by Roman_Hrytsyshyn.
 */
@RepositoryEventHandler(Account.class)
public class AccountEventHandler {

    @Autowired
    private AccountsEmitter accountsEmitter;

    @HandleAfterSave
    public void handleAfterSave(Account account) {
        accountsEmitter.emit(account);
    }
}