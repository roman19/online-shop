package com.epam.mp.hrytsyshyn.accountservice;

import com.epam.mp.hrytsyshyn.accountservice.repository.events.AccountEventHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@EnableDiscoveryClient
@SpringBootApplication
public class AccountServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceApplication.class, args);
    }

    @Bean
    AccountEventHandler accountEventHandler() {
        return new AccountEventHandler();
    }
}
