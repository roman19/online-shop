package com.epam.mp.hrytsyshyn.accountservice.repository.events;

import com.epam.mp.hrytsyshyn.accountservice.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by Roman_Hrytsyshyn.
 */
@Component
@EnableBinding(Source.class)
public class AccountsEmitter {

    private Source source;

    @Autowired
    public AccountsEmitter(Source source) {
        this.source = source;
    }

    public void emit(Account account) {
        source.output().send(MessageBuilder.withPayload(account).build());
    }
}