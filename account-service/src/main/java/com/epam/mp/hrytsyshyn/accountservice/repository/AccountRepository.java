package com.epam.mp.hrytsyshyn.accountservice.repository;

import com.epam.mp.hrytsyshyn.accountservice.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface AccountRepository extends MongoRepository<Account, String> {
}