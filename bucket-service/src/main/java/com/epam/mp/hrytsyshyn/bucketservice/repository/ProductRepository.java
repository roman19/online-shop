package com.epam.mp.hrytsyshyn.bucketservice.repository;

import com.epam.mp.hrytsyshyn.bucketservice.model.Product;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface ProductRepository extends GraphRepository<Product> {
    Product findByName(String name);

    Product findByMongoId(String mongoId);
}
