package com.epam.mp.hrytsyshyn.bucketservice.dto;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class BucketItemDTO {
    protected ProductDTO product;
    protected Integer quantity;

    public BucketItemDTO() {
    }

    public BucketItemDTO(ProductDTO product, Integer quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
