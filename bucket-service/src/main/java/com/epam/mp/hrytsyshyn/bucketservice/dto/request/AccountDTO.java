package com.epam.mp.hrytsyshyn.bucketservice.dto.request;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class AccountDTO {
    private String login;
    private String name;
    private String surname;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
