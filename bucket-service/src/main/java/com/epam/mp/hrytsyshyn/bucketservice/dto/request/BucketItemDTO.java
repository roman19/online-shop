package com.epam.mp.hrytsyshyn.bucketservice.dto.request;

import com.epam.mp.hrytsyshyn.bucketservice.model.Account;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class BucketItemDTO extends com.epam.mp.hrytsyshyn.bucketservice.dto.BucketItemDTO {
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
