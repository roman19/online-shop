package com.epam.mp.hrytsyshyn.bucketservice.endpoint;

import com.epam.mp.hrytsyshyn.bucketservice.dto.AccountDTO;
import com.epam.mp.hrytsyshyn.bucketservice.dto.request.BucketItemDTO;
import com.epam.mp.hrytsyshyn.bucketservice.repository.AccountRepository;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.DeletedBucketItem;
import com.epam.mp.hrytsyshyn.bucketservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Roman_Hrytsyshyn.
 */
@RefreshScope
@RestController
public class AccountRestController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/accounts/{login}")
    public AccountDTO getAccountBucketByLogin(@PathVariable String login) {
        return accountService.findByLogin(login);
    }

    @PostMapping("/accounts/{login}")
    public void addItemToBucket(@PathVariable String login, @RequestBody BucketItemDTO bucketItemDTO) {

        accountService.addProductToBucket(login, bucketItemDTO);
    }

    @DeleteMapping("/accounts/{login}/products/{id}")
    public DeletedBucketItem removeProductFromBucket(@PathVariable String login, @PathVariable("id") Long productId) {

        return accountService.removeProductFromBucket(login, productId);
    }
}
