package com.epam.mp.hrytsyshyn.bucketservice.service.impl;

import com.epam.mp.hrytsyshyn.bucketservice.dto.AccountDTO;
import com.epam.mp.hrytsyshyn.bucketservice.dto.request.BucketItemDTO;
import com.epam.mp.hrytsyshyn.bucketservice.exception.ResourceNotFoundException;
import com.epam.mp.hrytsyshyn.bucketservice.mapper.AccountDTOMapper;
import com.epam.mp.hrytsyshyn.bucketservice.model.Account;
import com.epam.mp.hrytsyshyn.bucketservice.model.BucketItem;
import com.epam.mp.hrytsyshyn.bucketservice.model.Product;
import com.epam.mp.hrytsyshyn.bucketservice.repository.AccountRepository;
import com.epam.mp.hrytsyshyn.bucketservice.repository.BucketItemRepository;
import com.epam.mp.hrytsyshyn.bucketservice.repository.ProductRepository;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.DeletedBucketItem;
import com.epam.mp.hrytsyshyn.bucketservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by Roman_Hrytsyshyn.
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private BucketItemRepository bucketItemRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public AccountDTO findByLogin(String login) {
        return Optional.of(accountRepository.findAccountBucketByLogin(login)).map(x -> AccountDTOMapper.map(x)).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public AccountDTO addProductToBucket(String login, BucketItemDTO bucketItemDTO) {
        Account account = Optional.ofNullable(accountRepository.findByLogin(login)).orElse(bucketItemDTO.getAccount());

        Product newProduct = Optional.ofNullable(productRepository.findByMongoId(bucketItemDTO.getProduct().getMongoId())).orElseGet(Product::new);

        newProduct.setMongoId(bucketItemDTO.getProduct().getMongoId());
        newProduct.setName(bucketItemDTO.getProduct().getName());
        newProduct.setPrice(bucketItemDTO.getProduct().getPrice());

        BucketItem newItem = new BucketItem();
        newItem.setAccount(account);
        newItem.setProduct(newProduct);
        newItem.setQuantity(bucketItemDTO.getQuantity());

        bucketItemRepository.save(newItem);

        return null;
    }

    @Override
    public DeletedBucketItem removeProductFromBucket(String login, Long productId) {
        return bucketItemRepository.deleteProductFromBucket(login, productId);
    }
}
