package com.epam.mp.hrytsyshyn.bucketservice.model;


import org.neo4j.ogm.annotation.*;

import static com.epam.mp.hrytsyshyn.bucketservice.model.Relations.HAS_IN_BUCKET;

/**
 * Created by Roman_Hrytsyshyn.
 */
@RelationshipEntity(type = HAS_IN_BUCKET)
public class BucketItem {
    @GraphId
    private Long id;
    @StartNode
    private Account account;
    @EndNode
    private Product product;

    @Property
    private Integer quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
