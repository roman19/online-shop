package com.epam.mp.hrytsyshyn.bucketservice.repository;

import com.epam.mp.hrytsyshyn.bucketservice.model.Account;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.AccountQueryResult;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import static com.epam.mp.hrytsyshyn.bucketservice.model.Relations.HAS_IN_BUCKET;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface AccountRepository extends GraphRepository<Account> {

    @Query("MATCH (a:Account {login:{0}}) OPTIONAL MATCH (a)-[r:" + HAS_IN_BUCKET + "]->(p:Product) RETURN a as account, collect(r) as bucketItems, collect(p) as products")
    AccountQueryResult findAccountBucketByLogin(String login);

    Account findByLogin(String login);
}