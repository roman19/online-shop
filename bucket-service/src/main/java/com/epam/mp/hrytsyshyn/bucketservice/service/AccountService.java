package com.epam.mp.hrytsyshyn.bucketservice.service;

import com.epam.mp.hrytsyshyn.bucketservice.dto.AccountDTO;
import com.epam.mp.hrytsyshyn.bucketservice.dto.request.BucketItemDTO;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.DeletedBucketItem;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface AccountService {
    AccountDTO findByLogin(String login);

    AccountDTO addProductToBucket(String login, BucketItemDTO bucketItemDTO);

    DeletedBucketItem removeProductFromBucket(String login, Long productId);
}
