package com.epam.mp.hrytsyshyn.bucketservice.repository.response;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by Roman_Hrytsyshyn.
 */
@QueryResult
public class DeletedBucketItem {
    private String productName;
    private Long quantity;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
