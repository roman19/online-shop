package com.epam.mp.hrytsyshyn.bucketservice.mapper;

import com.epam.mp.hrytsyshyn.bucketservice.dto.AccountDTO;
import com.epam.mp.hrytsyshyn.bucketservice.dto.BucketItemDTO;
import com.epam.mp.hrytsyshyn.bucketservice.dto.ProductDTO;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.AccountQueryResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class AccountDTOMapper {
    public static AccountDTO map(AccountQueryResult accountQueryResult) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setLogin(accountQueryResult.getAccount().getLogin());
        accountDTO.setName(accountQueryResult.getAccount().getName());
        accountDTO.setSurname(accountQueryResult.getAccount().getSurname());

        List<BucketItemDTO> bucketItems = accountQueryResult.getBucketItems().stream().map(x -> new BucketItemDTO(new ProductDTO(x.getProduct().getMongoId(), x.getProduct().getName(), x.getProduct().getPrice()), x.getQuantity())).collect(Collectors.toList());
        accountDTO.setBucketItems(bucketItems);

        return accountDTO;
    }
}
