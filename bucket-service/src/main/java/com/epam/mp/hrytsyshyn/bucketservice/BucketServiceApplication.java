package com.epam.mp.hrytsyshyn.bucketservice;

import com.epam.mp.hrytsyshyn.bucketservice.model.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@EnableDiscoveryClient
@EnableNeo4jRepositories
@SpringBootApplication

public class BucketServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BucketServiceApplication.class, args);
    }
}


