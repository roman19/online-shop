package com.epam.mp.hrytsyshyn.bucketservice.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class AccountDTO {
    private String login;
    private String name;
    private String surname;

    private List<BucketItemDTO> bucketItems = new ArrayList<>();

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<BucketItemDTO> getBucketItems() {
        return bucketItems;
    }

    public void setBucketItems(List<BucketItemDTO> bucketItems) {
        this.bucketItems = bucketItems;
    }

    public void addToBucket(BucketItemDTO item) {
        bucketItems.add(item);
    }
}
