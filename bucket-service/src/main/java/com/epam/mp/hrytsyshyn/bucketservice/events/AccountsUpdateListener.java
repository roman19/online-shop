package com.epam.mp.hrytsyshyn.bucketservice.events;

import com.epam.mp.hrytsyshyn.bucketservice.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;

import java.util.Optional;

/**
 * Created by Roman_Hrytsyshyn.
 */
@EnableBinding(UpdatedAccountsSource.class)
public class AccountsUpdateListener {

    @Autowired
    private AccountRepository accountRepository;

    @StreamListener(UpdatedAccountsSource.INPUT)
    public void handleUpdate(UpdatedAccount updatedAccount) {
        Optional.ofNullable(accountRepository.findByLogin(updatedAccount.getLogin())).ifPresent(
                oldAccount -> {
                    oldAccount.setName(updatedAccount.getName());
                    oldAccount.setSurname(updatedAccount.getSurname());
                    accountRepository.save(oldAccount);
                }
        );
    }
}

interface UpdatedAccountsSource {
    String INPUT = "updatedAccounts";

    @Input(INPUT)
    SubscribableChannel input();
}

class UpdatedAccount {
    private String login;
    private String name;
    private String surname;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
