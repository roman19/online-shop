package com.epam.mp.hrytsyshyn.bucketservice.repository.response;

import com.epam.mp.hrytsyshyn.bucketservice.model.Account;
import com.epam.mp.hrytsyshyn.bucketservice.model.BucketItem;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.Collection;

/**
 * Created by Roman_Hrytsyshyn.
 */
@QueryResult
public class AccountQueryResult {
    private Account account;
    private Collection<BucketItem> bucketItems;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Collection<BucketItem> getBucketItems() {
        return bucketItems;
    }

    public void setBucketItems(Collection<BucketItem> bucketItems) {
        this.bucketItems = bucketItems;
    }
}