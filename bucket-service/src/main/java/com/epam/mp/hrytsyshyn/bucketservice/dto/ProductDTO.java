package com.epam.mp.hrytsyshyn.bucketservice.dto;

/**
 * Created by Roman_Hrytsyshyn.
 */
public class ProductDTO {
    private String mongoId;
    private String name;
    private Double price;

    public ProductDTO() {
    }

    public ProductDTO(String mongoId, String name, Double price) {
        this.mongoId = mongoId;
        this.name = name;
        this.price = price;
    }

    public String getMongoId() {
        return mongoId;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}
