package com.epam.mp.hrytsyshyn.bucketservice.repository;

import com.epam.mp.hrytsyshyn.bucketservice.model.BucketItem;
import com.epam.mp.hrytsyshyn.bucketservice.repository.response.DeletedBucketItem;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import static com.epam.mp.hrytsyshyn.bucketservice.model.Relations.HAS_IN_BUCKET;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface BucketItemRepository extends GraphRepository<BucketItem> {
    @Query("MATCH (a:Account {login:{0}})-[r:" + HAS_IN_BUCKET + "]->(p:Product) where ID(p)={1} WITH r, r.quantity as quantity, p, p.name as productName DELETE r RETURN sum(quantity) as quantity, productName")
    DeletedBucketItem deleteProductFromBucket(String login, Long productId);
}