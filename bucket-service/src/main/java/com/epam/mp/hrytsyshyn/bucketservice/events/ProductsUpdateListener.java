package com.epam.mp.hrytsyshyn.bucketservice.events;

import com.epam.mp.hrytsyshyn.bucketservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;

import java.util.Optional;

/**
 * Created by Roman_Hrytsyshyn.
 */
@EnableBinding(UpdatedProductsSource.class)
public class ProductsUpdateListener {

    @Autowired
    ProductRepository productRepository;

    @StreamListener(UpdatedProductsSource.INPUT)
    public void handleUpdate(UpdatedProduct updatedProduct) {
        Optional.ofNullable(productRepository.findByMongoId(updatedProduct.getId())).ifPresent(
                oldProduct -> {
                    oldProduct.setName(updatedProduct.getName());
                    oldProduct.setPrice(updatedProduct.getPrice());
                    productRepository.save(oldProduct);
                }
        );
    }
}

interface UpdatedProductsSource {
    String INPUT = "updatedProducts";

    @Input(INPUT)
    SubscribableChannel input();
}


class UpdatedProduct {
    private String id;
    private String name;
    private Double price;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}