package com.epam.mp.hrytsyshyn.bucketservice.model;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by Roman_Hrytsyshyn.
 */
@NodeEntity
public class Product {
    @GraphId
    private Long id;
    private String mongoId;
    private String name;
    private Double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMongoId() {
        return mongoId;
    }

    public String getName() {
        return name;
    }

    public void setMongoId(String mongoId) {
        this.mongoId = mongoId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
