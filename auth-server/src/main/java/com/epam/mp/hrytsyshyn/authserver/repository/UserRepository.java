package com.epam.mp.hrytsyshyn.authserver.repository;

import com.epam.mp.hrytsyshyn.authserver.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByLogin(String login);

}
