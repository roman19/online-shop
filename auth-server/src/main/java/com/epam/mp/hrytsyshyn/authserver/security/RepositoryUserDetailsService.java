package com.epam.mp.hrytsyshyn.authserver.security;

import com.epam.mp.hrytsyshyn.authserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Roman_Hrytsyshyn.
 */
@Service
public class RepositoryUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByLogin(username).map(x -> new UserDetailsDecorator(x)).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}