package com.epam.mp.hrytsyshyn.productservice.repository.event;

import com.epam.mp.hrytsyshyn.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

/**
 * Created by Roman_Hrytsyshyn.
 */
@RepositoryEventHandler(Product.class)
public class ProductEventHandler {

    @Autowired
    private ProductsEmitter productsEmitter;

    @HandleAfterSave
    public void handleAfterSave(Product product) {
        productsEmitter.emit(product);
    }

}