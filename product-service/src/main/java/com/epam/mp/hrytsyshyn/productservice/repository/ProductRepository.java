package com.epam.mp.hrytsyshyn.productservice.repository;

import com.epam.mp.hrytsyshyn.productservice.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Roman_Hrytsyshyn.
 */
public interface ProductRepository extends MongoRepository<Product, String> {

    List<Product> findByName(@Param("name") String name);
}