package com.epam.mp.hrytsyshyn.productservice.repository.event;

import com.epam.mp.hrytsyshyn.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by Roman_Hrytsyshyn.
 */
@Component
@EnableBinding(Source.class)
public class ProductsEmitter {

    private Source source;

    @Autowired
    public ProductsEmitter(Source source) {
        this.source = source;
    }

    public void emit(Product product) {
        source.output().send(MessageBuilder.withPayload(product).build());
    }
}